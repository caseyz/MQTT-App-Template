// This #include statement was automatically added by the Particle IDE.
#include "SparkJson.h"
#include "MQTT.h"

char mqttBroker[32] = "ctfhosts.ddns.net";  //MQTT Broker URL / IP
String mqttPub = "garden/soilMoisture";     //MQTT Publication Channel
String mqttSub = "garden/soilMoisture/sub"; //MQTT Subscription Channel
String mqttLog = "log/";                    //MQTT logging channel
String value = "0";                         //Initialize reporting value
int wait = 15000;                           //Time between loops
String myID;                                //Variable for the Photon device ID
String strMqtt;                             //Variable to contain the MQTT string
int str_len;                                //Variable for String Length
int counter = 0;                            //Variable to count time for reporting
int reportDelay = 60000;                    //Time Between reports

int m;                                      //Moisture reading
int a = 0;                                  //Averaged reading
int c;                                      //Count of reads

int rssi;                                   //RSSI strength variable

int led = D7;                               //Which LED to blink

MQTT client(mqttBroker, 1883, callback);    //Initialized MQTT broker

void setup()
    {
        Serial.begin(9600);

        //Get the deviceID
        myID = System.deviceID();
        strMqtt = mqttLog;

        //myID data conversion ========================================================

        //Convert the string to Char for MQTT
        //Get the length of the string

        str_len = myID.length() + 1;

        //Prepare the character array (the buffer)
        char char_array3[str_len];

        // Copy it over
        myID.toCharArray(char_array3, str_len);

        //END myID data conversion ========================================================

        //Create json status object
        StaticJsonBuffer<200> jsonBuffer;
        char buffer [200];

        JsonObject& root = jsonBuffer.createObject();
        root["deviceID"] = char_array3;
        root["status"] = "Connected at startup";


        root.printTo(Serial);
        root.printTo(buffer, sizeof(buffer));

        //Set pin modes

        pinMode(led, OUTPUT);

        // connect to the broker
        client.connect("connect");


        //Publish our status json to the broker
        client.publish(strMqtt, buffer);

        //Subscribe to the broker to recieve messages
        client.subscribe(mqttSub);
    }


void loop()
    {
        heartbeat();
        blink(1);

        //Report Timer
        if (millis()>counter)
            {
                //Reset variables
                m = 0;
                a = 0;
                c = 0;

                readSensor(64);

                //Publish to the MQTT Broker
                report(m, mqttPub);

                counter = counter + reportDelay;
            }

        client.loop();
        delay(wait);

    }

//Function handles reporting to the MQTT broker
void report(int m, String feed)
    {
        //Value data type conversion ====================================================
        //Convert the int value to a string
        String stringData;
        stringData = String(m);

        //Convert the string to Char for MQTT
        //Get the length of the string
        int str_len;
        str_len = stringData.length() + 1;

        //Prepare the character array (the buffer)
        char char_array[str_len];

        // Copy it over
        stringData.toCharArray(char_array, str_len);

        //END Value data conversion =====================================================

        //RSSI data conversion ==========================================================
        //Get rssi and publish
        rssi = WiFi.RSSI();

        //Convert the int value to a string
        String stringRssi;
        stringRssi = String(rssi);

        //Convert the string to Char for MQTT
        //Get the length of the string

        str_len = stringRssi.length() + 1;

        //Prepare the character array (the buffer)
        char char_array2[str_len];

        // Copy it over
        stringRssi.toCharArray(char_array2, str_len);
        //END RSSI data conversion ====================================================

        //myID data conversion ========================================================

        //Convert the string to Char for MQTT
        //Get the length of the string

        str_len = myID.length() + 1;

        //Prepare the character array (the buffer)
        char char_array3[str_len];

        // Copy it over
        myID.toCharArray(char_array3, str_len);

        //END myID data conversion ========================================================

        //Build json REPORT object

        StaticJsonBuffer<200> jsonBuffer;
        char bufferReport [200];

        JsonObject& root = jsonBuffer.createObject();
        root["deviceID"] = char_array3;
        root["report"] = char_array;
        root["rssi"] = char_array2;

        root.printTo(Serial);
        root.printTo(bufferReport, sizeof(bufferReport));

        if (client.isConnected())
            {
                //myID data conversion ========================================================

                //Convert the string to Char for MQTT
                //Get the length of the string

                str_len = myID.length() + 1;

                //Prepare the character array (the buffer)
                char char_array3[str_len];

                // Copy it over
                myID.toCharArray(char_array3, str_len);

                //END myID data conversion ========================================================

                //Create json STATUS object
                StaticJsonBuffer<200> jsonBuffer;
                char buffer [200];

                JsonObject& root = jsonBuffer.createObject();
                root["deviceID"] = char_array3;
                root["status"] = "Connected during report";
                root.printTo(buffer, sizeof(buffer));

                client.publish(strMqtt, buffer);

                client.publish(feed,bufferReport);
                blink(3);

            }
            else
            {
                client.connect("connect");
                if (client.isConnected())
                    {
                        //myID data conversion ========================================================

                        //Convert the string to Char for MQTT
                        //Get the length of the string

                        str_len = myID.length() + 1;

                        //Prepare the character array (the buffer)
                        char char_array3[str_len];

                        // Copy it over
                        myID.toCharArray(char_array3, str_len);

                        //END myID data conversion ========================================================

                        //Create json status object
                        StaticJsonBuffer<200> jsonBuffer;
                        char buffer [200];

                        JsonObject& root = jsonBuffer.createObject();
                        root["deviceID"] = char_array3;
                        root["status"] = "Disconnected at time of report.  Reconnecting";
                        root.printTo(buffer, sizeof(buffer));

                        client.publish(strMqtt, buffer);

                        client.publish(feed,bufferReport);
                        blink(10);
                    }
                }
            }

// Allows us to recieve a message from the subscription
void callback(char* topic, byte* payload, unsigned int length)
    {
        char p[length + 1];
        memcpy(p, payload, length);
        p[length] = NULL;
        String message(p);

    //This is where you put code to handle any message recieved from the broker

    }

void blink(int blinks)
    {

        int x = 0;

        do
        {
          digitalWrite(led, HIGH);
          delay(100);
          digitalWrite(led, LOW);
          delay(100);
          x = x + 1;

        } while (x < blinks);
    }

void heartbeat()
    {
        //myID data conversion ========================================================

        //Convert the string to Char for MQTT
        //Get the length of the string

        str_len = myID.length() + 1;

        //Prepare the character array (the buffer)
        char char_array3[str_len];

        // Copy it over
        myID.toCharArray(char_array3, str_len);

        //END myID data conversion ========================================================

        //Create json status object
        StaticJsonBuffer<200> jsonBuffer;
        char buffer [200];

        JsonObject& root = jsonBuffer.createObject();
        root["deviceID"] = char_array3;
        root["status"] = "Heartbeat";


        root.printTo(Serial);
        root.printTo(buffer, sizeof(buffer));

        client.publish(strMqtt, buffer);

    }

int readSensor(int n)
    {
        //Read the sensor n times
        for (int i=0; i <= n; i++)
            {
                m = analogRead(A0);
                Serial.println(m);

                a = a + m;
                c = i;

                delay(5);
            }

        //Average the readings
        m = a / c;

        return m;
    }
